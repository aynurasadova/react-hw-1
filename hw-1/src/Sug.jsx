import React, { useState, useEffect } from 'react';
import Button from './components/Button';
import { Modal } from './components/Modal';
import './styles.scss'

export const Sug = () => {
  const [modalStatus, setModalStatus] = useState(false);

  const [isActive, setActive] = useState(false);

//   const toggleFirstModal = () => {
//     setFirstModalStatus(v => !v)//v for previous
//     setActive(h => !h);
//     // isActive ?  document.body.style.backgroundColor = "": document.body.style.backgroundColor = "black"
//   }

//   const toggleSecondModal = () => {
//     setSecondModalStatus(v =>!v)
//     setActive(h => !h);
//   }


  // useEffect(() => {
    
  //   if(isActive === true) {
  //     document.body.style.backgroundColor = "black";
  //   }
  //   else {
  //     document.body.style.backgroundColor = "#cbcb54"
  //   }
    
  // });
  
  // document.body.style.position = "relative";

  return (
    <div className={(isActive) ? "App-black" : "App"} onClick = {(firstModalStatus ? toggleFirstModal: '')}>
      <div className = {isActive ? "display-none" : "app-buttons"}>
     
      <Button 
        backgroundColor='magenta' 
        text='Open first modal' 
        onClick={() => setModalStatus(v => !v)}/>

      <Button
        backgroundColor='green' 
        text='Open second modal' 
        onClick={() => setModalStatus(v => !v)}/>
      </div>
      <div>
            <div id = "modal-1">
            {(modalStatus&& (
                <Modal
                  
                  header="Do you want to delete this file?"
                  closeIcon = {true}
                  text = "Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete this file?"
                  backgroundColor = "#e74c3c"
                  close = {() => setModalStatus(v => !v)}
                  actions = {[
                    <Button 
                      key = {1}
                      backgroundColor='rgba(0,0,0,.3)' 
                      text='Ok' 
                      onClick={() =>alert('You have deleted succesfully!')}/>,
                    <Button 
                      key ={2}
                      backgroundColor='rgba(0,0,0,.3)' 
                      text='Cancel' 
                      onClick={() => setModalStatus(v => !v)}/>]}
                     
                    />

                                )
                                
              )
            } 
            
            </div>
              
              <div className = "modal-2">
              {modalStatus && (
                <div style = {{zIndex: 1, backgroundColor: "yellow"}}  onClick = {() => (null)}>
                  <Modal
                    header = "Have a cookie :)"
                    closeIcon = {false}
                    text = "We use cookies to ensure that we give you the best experience on our website. We also use cookies to ensure we show you advertising that is relevant to you. "
                    close = {() => setModalStatus(v => !v)}
                    backgroundColor = "#5fdcdc"
                    actions = {[
                        <Button 
                          key = {3}
                          backgroundColor = 'red'
                          text = 'Allow'
                          onClick = { () => alert('Thank you!')}
                        />,
                        <Button 
                          key = {4}
                          backgroundColor = 'blue'
                          text = "Decline"
                          onClick = {() => setModalStatus(v => !v)}
                        />
                      ]
                    }
                  
                  />
                </div>
                  
                )
              }
              </div>
      </div>
      
        

      
    </div>
  );
}

