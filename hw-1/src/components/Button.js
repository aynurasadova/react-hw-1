import React, {Component} from 'react';
import PropTypes from 'prop-types';
import '../styles.scss'
const Button = ({text, backgroundColor,onClick}) => {
    return (
        <div>
           <button 
            className="btn" 
            style={{backgroundColor}}
            onClick={onClick}
           >
               {text}
           </button>
        </div>
    )
}

Button.propTypes = {
    text: PropTypes.string,
    backgroundColor: PropTypes.string,
    onClick: PropTypes.func
}
// <button style={{backgroundColor: props.backgroundColor}} onClick={() => props.changeModal()}>
// { props.text }
// </button>

export default Button;