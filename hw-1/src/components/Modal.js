import React from 'react';
import PropTypes from 'prop-types';
import '../styles.scss'
import close from './close.svg'
export const Modal = ({header,closeIcon,actions, text, close, backgroundColor, block}) => {
    
    return(
        <div 
        onClick = {block}
        className = "modal" 
        style={{backgroundColor}}>
                <header className = "modal-head">
                    {header}
                    {closeIcon && <button onClick = {close} className="close-btn">X</button>}
                </header>

                <main className = "modal-main">
                    <p className = "modal-text">{text}</p>
                    <div className = "buttons">
                        {actions}
                    </div>
                    
                </main>
        
        </div>
    
    )
}