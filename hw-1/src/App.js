import React, { useState, useEffect } from 'react';
import Button from './components/Button';
import { Modal } from './components/Modal';
import './styles.scss'
function App() {
  const [firstModalStatus, setFirstModalStatus] = useState(false);
  const [secondModalStatus, setSecondModalStatus] = useState(false);

  const [isActive, setActive] = useState(false);

  const toggleFirstModal = () => {
    setFirstModalStatus(v => !v)//v for previous
    setActive(j =>!j)
  }

  const toggleSecondModal = () => {
    setSecondModalStatus(v =>!v)
    setActive(j =>!j)
  }


  const stopPropagation = (e) => {
    e.stopPropagation();
 }

  return (
    <div className= "main-app">
      <div className = {(isActive) ? "display-none" : "app-buttons"}>
        <Button 
          backgroundColor='magenta' 
          text='Open first modal' 
          onClick={toggleFirstModal}/>

        <Button
          backgroundColor='green' 
          text='Open second modal' 
          onClick={toggleSecondModal}/>
      </div>

      <div className = "modals-container">  
        <div className = { isActive? "App-black":"App"} onClick = {isActive?toggleFirstModal: ''}>
              {(firstModalStatus&& (
                
                  <Modal
                    block = {stopPropagation}
                    header="Do you want to delete this file?"
                    closeIcon = {true}
                    text = "Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete this file?"
                    backgroundColor = "#e74c3c"
                    close = {toggleFirstModal}
                    actions = {[
                      <Button 
                        key = {1}
                        backgroundColor='rgba(0,0,0,.3)' 
                        text='Ok' 
                        onClick={() =>{
                          alert('You have deleted succesfully!')
                          toggleFirstModal()
                    }}/>,
                      <Button 
                        key ={2}
                        backgroundColor='rgba(0,0,0,.3)' 
                        text='Cancel' 
                        onClick={toggleFirstModal}/>]}
                      
                      />

                                  )
                                  
                )
              } 
              
              </div>
                
                <div className = { isActive? "App-black":"App"} onClick = {isActive? toggleSecondModal: ''}>
                {secondModalStatus && (
                    <Modal
                      block = {stopPropagation}
                      header = "Have a cookie :)"
                      closeIcon = {false}
                      text = "We use cookies to ensure that we give you the best experience on our website. We also use cookies to ensure we show you advertising that is relevant to you. "
                      close = {toggleSecondModal}
                      backgroundColor = "#5fdcdc"
                      actions = {[
                          <Button 
                            key = {3}
                            backgroundColor = 'red'
                            text = 'Allow'
                            onClick = { () => {
                              alert('Thank you!')
                              toggleSecondModal()
                          }}
                          />,
                          <Button 
                            key = {4}
                            backgroundColor = 'blue'
                            text = "Decline"
                            onClick = {toggleSecondModal}
                          />
                        ]
                      }
                    
                    />
                  )
                }
        </div>
        
      </div>
        

      
    </div>
  );
}

export default App;
